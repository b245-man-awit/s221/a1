// console.log("Hello")

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [
    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList =
    [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

function register(string) {
    if (registeredUsers.includes(string)) {
        alert("Registration failed. Username already exists!")
    } else {
        registeredUsers.push(string);
        alert("Thank you for registering!")
    }
}

register('Sharmaine Man-awit')
console.log(registeredUsers);



/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
function addFriend(string) {
    if (registeredUsers.includes(string)) {
        friendsList.push(string);
    }
    else {
        alert("User not found.");
    }

}
addFriend("Akiko Yukihime")
console.log(friendsList);


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
function showFriend() {
    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    }
    else {
        console.log(friendsList)
    }
}
showFriend()



/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
function showRegisterAmount() {
    if (friendsList.length !== 0) {
        alert("You currently have " + friendsList.length + " friends")
    }
    else {
        alert("You currently have 0 friends. Add one first.")
    }

}
showRegisterAmount();



/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
    function deleteFriend(){

        if (friendsList.length === 0) {
            alert ("You currently have 0 friends. Add one first.");
        }
        else {
            friendsList.splice(friendsList.length-1,1)
        }
        
    }
    deleteFriend()
    console.log(friendsList);
  

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

    function deleteSpecificFriend(user){
        if (friendsList.length === 0) {
            alert ("You currently have 0 friends. Add one first.");
        }
        else {
            let index = friendsList.indexOf(user);
            friendsList.splice(index,1);
            
        }


    }
    addFriend("Akiko Yukihime")
    addFriend("James Jeffries")
    addFriend("Fernando Dela Cruz")
    console.log(friendsList);
    deleteSpecificFriend("James Jeffries")
    console.log(friendsList);
